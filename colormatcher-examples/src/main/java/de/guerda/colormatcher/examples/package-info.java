/**
 * Contains all examples for the colormatcher library  
 * @author Philip guerda Gillißen
 *
 */
package de.guerda.colormatcher.examples;