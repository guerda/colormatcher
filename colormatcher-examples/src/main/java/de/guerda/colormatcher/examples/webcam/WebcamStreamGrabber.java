package de.guerda.colormatcher.examples.webcam;

import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebcamStreamGrabber {

  private OpenCVFrameGrabber frameGrabber;
  private Logger logger;

  public WebcamStreamGrabber(int aDeviceNumber) throws Exception {
    frameGrabber = new OpenCVFrameGrabber(aDeviceNumber);
    frameGrabber.start();
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  protected Frame getImage() {
    try {
      Frame tmpImage = frameGrabber.grab();
      return tmpImage;
    } catch (Exception e) {
      getLogger().error("Could not grab frame from video device", e);
    }

    return null;
  }

  public int getImageWidth() {
    return frameGrabber.getImageWidth();
  }

  public int getImageHeight() {
    return frameGrabber.getImageHeight();
  }

}
