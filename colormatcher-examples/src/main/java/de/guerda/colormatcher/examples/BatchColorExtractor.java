package de.guerda.colormatcher.examples;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;

public class BatchColorExtractor {

  private ImageMCCQ imageMCCQ;
  private Logger logger;
  private int count;
  private long accumulatedDuration;
  private BufferedWriter bufferedWriter;
  private FileWriter fileWriter;

  public static void main(String[] args) {
    if (args.length != 2) {
      System.err.println("Usage: BatchColorExtractor [folder of images] [target csv]");
      System.exit(1);
    }
    String tmpFolder = args[0];
    String tmpOutputCsvFile = args[1];
    BatchColorExtractor tmpBatchColorExtractor = new BatchColorExtractor();
    tmpBatchColorExtractor.initializeCsvOutput(tmpOutputCsvFile);
    tmpBatchColorExtractor.extractColorsFromImagesInFolder(tmpFolder);
    tmpBatchColorExtractor.closeCsvOutput();
  }

  public BatchColorExtractor() {
    imageMCCQ = new ImageMCCQ();
  }

  private void initializeCsvOutput(String aFileName) {
    try {
      fileWriter = new FileWriter(new File(aFileName));
      bufferedWriter = new BufferedWriter(fileWriter);
      bufferedWriter.write(String.format("Red 1;Green 1;Blue 1;Red 2;Green 2;Blue 2;Match%n", ""));
    } catch (IOException e) {
      getLogger().error("Could not initialize file output for file '" + aFileName + "'", e);
      System.exit(1);
    }

  }

  private void closeCsvOutput() {
    if (bufferedWriter != null) {
      try {
	bufferedWriter.close();
      } catch (IOException e) {
	getLogger().debug("Could not close BufferedWriter", e);
      }
    }

    if (fileWriter != null) {
      try {
	fileWriter.close();
      } catch (IOException e) {
	getLogger().debug("Could not close FileWriter", e);
      }
    }

  }

  private void extractColorsFromImagesInFolder(String aFolder) {
    count = 0;
    accumulatedDuration = 0;
    Path tmpPath = Paths.get(aFolder);
    String tmpImageFileTypePattern = "glob:*.{jpg,jpeg,png,bmp}";
    final PathMatcher tmpMatcher = FileSystems.getDefault().getPathMatcher(tmpImageFileTypePattern);
    try {
      Files.walkFileTree(tmpPath, new SimpleFileVisitor<Path>() {
	@Override
	public FileVisitResult visitFile(Path aFile, BasicFileAttributes aAttrs) throws IOException {
	  Path tmpFileName = aFile.getFileName();
	  if (aAttrs.isRegularFile() && tmpFileName != null && tmpMatcher.matches(tmpFileName)) {
	    extractColorsFromImage(aFile);
	  }
	  return FileVisitResult.CONTINUE;
	}

      });
    } catch (IOException e) {
      getLogger().error("Could not list files in directors '" + aFolder + "'", e);
    }
    if (count > 0) {
      getLogger().info("Done, average duration per image: " + (accumulatedDuration / count) + "ms");
    }
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  private void extractColorsFromImage(Path aFile) {
    count++;
    getLogger().info("Extracting colors from " + count + "th file '" + aFile.toString() + "'...");
    BufferedImage tmpRead;
    try {
      tmpRead = ImageIO.read(aFile.toFile());
      long tmpStart = System.currentTimeMillis();
      List<CustomColor> tmpDominantColors = imageMCCQ.getDominantColors(tmpRead, 4);
      StringBuffer tmpColorString = new StringBuffer();
      for (CustomColor tmpCustomColor : tmpDominantColors) {
	tmpColorString.append(tmpCustomColor.toString() + " ");
      }
      getLogger().info(tmpColorString.toString());
      long tmpDuration = System.currentTimeMillis() - tmpStart;
      getLogger().info("Duration: " + tmpDuration + " ms");
      accumulatedDuration += tmpDuration;

      writeColorsToCsv(tmpDominantColors);
    } catch (IOException e) {
      getLogger().error("Could not read file '" + aFile.getFileName() + "'", e);
    }
  }

  private void writeColorsToCsv(List<CustomColor> aDominantColors) {
    try {
      for (int i = 0; i < aDominantColors.size(); i++) {
	CustomColor tmpCustomColor = aDominantColors.get(0);
	for (int j = i + 1; j < aDominantColors.size(); j++) {
	  CustomColor tmpCustomColor2 = aDominantColors.get(j);
	  writeColorPairToCsv(tmpCustomColor, tmpCustomColor2);
	}
      }
    } catch (IOException e) {
      getLogger().error("Could not write row to CSV!", e);
    }
  }

  private void writeColorPairToCsv(CustomColor aCustomColor1, CustomColor aCustomColor2) throws IOException {
    String tmpRow = String.format("%d;%d;%d;%d;%d;%d;%d%n",
	aCustomColor1.getRed(), aCustomColor1.getGreen(), aCustomColor1.getBlue(),
	aCustomColor2.getRed(), aCustomColor2.getGreen(), aCustomColor2.getBlue(),
	1);
    bufferedWriter.write(tmpRow);
    bufferedWriter.flush();
  }
}
