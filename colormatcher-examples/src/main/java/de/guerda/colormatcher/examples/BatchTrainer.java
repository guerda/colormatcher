package de.guerda.colormatcher.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;
import de.guerda.colormatcher.core.matching.ColorMatcher;
import de.guerda.colormatcher.core.matching.NetworkPersistenceError;

public class BatchTrainer {

  private Logger logger;
  private String inputCsvFileName;
  private String outputNetworkFileName;
  private ColorMatcher colorMatcher;

  public static void main(String[] args) {
    if (args.length != 2) {
      System.err.println("Usage: BatchTrainer [input csv created with BatchColorExtractor] [output network file name]");
      System.exit(1);
    }

    new BatchTrainer(args[0], args[1]).run();
  }

  public BatchTrainer(String anInputCsvFileName, String anOutputNetworkFileName) {
    inputCsvFileName = anInputCsvFileName;
    outputNetworkFileName = anOutputNetworkFileName;
    colorMatcher = new ColorMatcher();
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  private void run() {
    File tmpInputFile = new File(inputCsvFileName);
    getLogger().info("Train network from CSV file '" + inputCsvFileName + "'...");
    boolean tmpIsFirstLine = true;
    try (BufferedReader tmpReader = new BufferedReader(new InputStreamReader(new FileInputStream(tmpInputFile)))) {
      String tmpLine = null;
      while ((tmpLine = tmpReader.readLine()) != null) {
	if (tmpIsFirstLine) {
	  tmpIsFirstLine = false;
	  continue;
	}
	String[] tmpSplit = tmpLine.split(";");
	// TODO throw Exception if not enough values in line
	CustomColor tmpCustomColor1 = convertStringsToCustomColor(tmpSplit[0], tmpSplit[1], tmpSplit[2]);
	CustomColor tmpCustomColor2 = convertStringsToCustomColor(tmpSplit[3], tmpSplit[4], tmpSplit[5]);
	double tmpMatch = Double.parseDouble(tmpSplit[6]);
	colorMatcher.addPairAndTrainNetwork(tmpCustomColor1, tmpCustomColor2, tmpMatch);
      }
    } catch (FileNotFoundException e) {
      getLogger().error("Could not find file '" + inputCsvFileName + "'!", e);
    } catch (IOException e) {
      getLogger().error("Could not read from file '" + inputCsvFileName + "'!", e);
    }
    getLogger().info("Done training");

    getLogger().info("Write network to file '" + outputNetworkFileName + "'...");
    try {
      colorMatcher.saveNetworkToStream(new FileOutputStream(new File(outputNetworkFileName)));
    } catch (FileNotFoundException | NetworkPersistenceError e) {
      getLogger().error("Could not save network to file '" + outputNetworkFileName + "'!", e);
    }
    getLogger().info("Done");

  }

  private CustomColor convertStringsToCustomColor(String aRed, String aBlue, String aGreen) {
    int tmpRed = Integer.parseInt(aRed);
    int tmpGreen = Integer.parseInt(aGreen);
    int tmpBlue = Integer.parseInt(aBlue);
    return new CustomColor(tmpRed, tmpGreen, tmpBlue);
  }
}
