package de.guerda.colormatcher.examples;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import de.guerda.colormatcher.core.CustomColor;
import de.guerda.colormatcher.core.extraction.MedianCutColorQuantization;

public class ImageMCCQ extends MedianCutColorQuantization {

  /**
   * Determines n dominant colors from a given image.
   * 
   * @param anImage
   *          the image to inspect, as a {@link BufferedImage}.
   * @param aCountOfColors
   *          the count of colors. Should be a 2^n (e.g. 2, 4, 8, 16 etc).
   * @return a list of dominant colors
   */
  public List<CustomColor> getDominantColors(final BufferedImage anImage,
      final int aCountOfColors) {
    return getDominantColors(anImage, aCountOfColors, DEFAULT_QUALITY);
  }

  /**
   * Determines n dominant colors from a given image.
   * 
   * @param anImage
   *          the image to inspect, as a {@link BufferedImage}.
   * @param aCountOfColors
   *          the count of colors. Should be a 2^n (e.g. 2, 4, 8, 16 etc).
   * @param aQuality
   *          an indicator of quality of the determination. Ranges from 1-
   *          {@link #MAX_QUALITY}.
   * @return a list of dominant colors
   */
  public List<CustomColor> getDominantColors(final BufferedImage anImage,
      final int aCountOfColors, final int aQuality) {
    if (anImage == null) {
      String tmpMessage = "Given image was null. Please provide a"
	  + "valid BufferedImage!";
      throw new IllegalArgumentException(tmpMessage);
    }

    int tmpCountOfPixels = anImage.getWidth() * anImage.getHeight();
    if (aCountOfColors > tmpCountOfPixels) {
      throw new IllegalArgumentException("Requested count of colors was larger"
	  + " than the image's count of pixels (" + tmpCountOfPixels
	  + "). Please provide a smaller count of colors!");
    }

    ArrayList<CustomColor> tmpColors = new ArrayList<>();

    int tmpWidth = anImage.getWidth();
    int tmpHeight = anImage.getHeight();

    for (int i = 0; i < tmpWidth; i++) {
      for (int j = 0; j < tmpHeight; j++) {
	int tmpRgb = anImage.getRGB(i, j);
	CustomColor tmpColor = new CustomColor(tmpRgb);
	tmpColors.add(tmpColor);
      }
    }

    return getDominantColors(tmpColors, aCountOfColors, aQuality);
  }

}
