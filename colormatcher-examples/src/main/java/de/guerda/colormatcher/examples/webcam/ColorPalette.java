package de.guerda.colormatcher.examples.webcam;

import java.awt.Color;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.guerda.colormatcher.core.CustomColor;

/**
 * This component can show a list of colors directly in the JPanel.
 * 
 * @author Philip guerda Gillißen
 *
 */
public class ColorPalette extends JPanel {

  /**
   * Default serial version UID.
   */
  private static final long serialVersionUID = 1L;
  private int colorBoxSize = 50;

  public ColorPalette() {
    super();
    setBorder(new EmptyBorder(0, 0, 0, 0));
  }

  /**
   * Display a new list of colors in the panel.
   * 
   * @param aColorList
   *          the list of colors to be displayed
   */
  public void showColors(List<CustomColor> aColorList) {
    removeAll();
    for (CustomColor tmpColor : aColorList) {
      addPanelWithColor(tmpColor);
    }
    revalidate();
  }

  /**
   * Creates and adds a new color panel in the palette
   * 
   * @param aColor
   */
  private void addPanelWithColor(CustomColor aColor) {
    JPanel tmpJPanel = new JPanel();
    tmpJPanel.setMinimumSize(new Dimension(colorBoxSize, colorBoxSize));
    tmpJPanel.setPreferredSize(new Dimension(colorBoxSize, colorBoxSize));
    tmpJPanel.setBackground(new Color(aColor.getRed(), aColor.getGreen(), aColor.getBlue()));
    add(tmpJPanel);

  }

  public void setColorBoxSize(int aColorBoxSize) {
    colorBoxSize = aColorBoxSize;
  }
}
