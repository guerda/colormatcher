/**
 * 
 */
package de.guerda.colormatcher.examples.webcam;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.Timer;

import net.miginfocom.swing.MigLayout;

import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;
import de.guerda.colormatcher.examples.ImageMCCQ;

/**
 * This example shows how to extract images from a webcam and extract the
 * image's dominant colors.
 * 
 * @author Philip guerda Gillißen
 *
 */
public class WebcamExample {

  private static final Dimension FRAME_SIZE = new Dimension(800, 720);
  private CanvasFrame frame;
  private Logger logger;
  private WebcamStreamGrabber webcamStreamGrabber;
  private ImageMCCQ imageMCCQ;
  private Java2DFrameConverter converter;
  private ColorPalette dominantColors;

  /**
   * This main method starts the camera, shows a live stream and immediately
   * determines the dominant colors of the live stream.
   * 
   * @param args
   */
  public static void main(String[] args) {
    WebcamExample tmpWebcamExample = new WebcamExample();
    tmpWebcamExample.run();
  }

  public WebcamExample() {
    imageMCCQ = new ImageMCCQ();
    converter = new Java2DFrameConverter();
    initializeComponents();
    initializeWebcam();
  }

  private void initializeComponents() {
    frame = new CanvasFrame("WebcamExample for ColorMatcher");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(FRAME_SIZE);
    frame.setLayout(new MigLayout());

    // Add components
    Container tmpContentPane = frame.getContentPane();
    // to have a break after the webcam image
    Component tmpComponent = tmpContentPane.getComponent(0);
    tmpContentPane.remove(0);
    tmpContentPane.add(tmpComponent, "wrap");
    // Add Palette of dominant colors
    dominantColors = new ColorPalette();
    tmpContentPane.add(dominantColors);


    // Show the windows
    frame.setVisible(true);
  }

  private void run() {
    Timer tmpTimer = new Timer(0, new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent aE) {
	Frame tmpImage;
	tmpImage = webcamStreamGrabber.getImage();
	if (tmpImage != null) {
	  // Show video frame in canvas
	  frame.showImage(tmpImage);
	  BufferedImage tmpBufferedImage = converter.convert(tmpImage);
	  List<CustomColor> tmpDominantColors = imageMCCQ.getDominantColors(tmpBufferedImage, 8);
	  dominantColors.showColors(tmpDominantColors);
	}
	((Timer) aE.getSource()).restart();
      }
    });
    tmpTimer.setDelay(1);
    tmpTimer.start();
  }

  private void initializeWebcam() {
    try {
      webcamStreamGrabber = new WebcamStreamGrabber(0);
      // Set canvas size as per dimensions of video frame.
      frame.setCanvasSize(webcamStreamGrabber.getImageWidth(), webcamStreamGrabber.getImageHeight());
      frame.setSize(FRAME_SIZE);
    } catch (Exception e) {
      getLogger().error("Could not access webcam", e);
    }
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

}
