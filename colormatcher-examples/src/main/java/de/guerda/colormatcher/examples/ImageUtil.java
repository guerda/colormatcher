package de.guerda.colormatcher.examples;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class ImageUtil {

  /**
   * Scales an image and keeps the ratio.
   * 
   * @param aSourceImage
   * @param aMaximumWidth
   * @param aMaximumHeight
   * @return
   */
  public static Image getScaledImage(BufferedImage aSourceImage, int aMaximumWidth, int aMaximumHeight) {
    int tmpNewHeight = 0;
    int tmpNewWidth = 0;
    if (aSourceImage.getWidth() > aSourceImage.getHeight()) {
      tmpNewWidth = aMaximumWidth;
      tmpNewHeight = tmpNewWidth * aSourceImage.getHeight() / aSourceImage.getWidth();
    } else {
      tmpNewHeight = aMaximumHeight;
      tmpNewWidth = tmpNewHeight * aSourceImage.getWidth() / aSourceImage.getHeight();
    }
    BufferedImage resizedImg = new BufferedImage(tmpNewWidth, tmpNewHeight, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2 = resizedImg.createGraphics();

    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g2.drawImage(aSourceImage, 0, 0, tmpNewWidth, tmpNewHeight, null);
    g2.dispose();

    return resizedImg;
  }

}
