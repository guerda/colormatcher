package de.guerda.colormatcher.examples;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;
import de.guerda.colormatcher.core.matching.ColorMatcher;
import de.guerda.colormatcher.core.matching.NetworkPersistenceError;
import net.miginfocom.swing.MigLayout;

/**
 * Hello world!
 *
 */
public final class ColorMatcherExample {

  /**
   * This member contains the class specific logger.
   */
  private Logger logger;

  /**
   * This member contains the color panel for the first color.
   */
  private JPanel color1Panel;

  /**
   * This member contains the color panel for the second color.
   */
  private JPanel color2Panel;

  /**
   * This member contains the slider with which the user defines the match.
   */
  private JSlider matchSlider;

  /**
   * This member contains the color matcher which learns and determines the
   * match.
   */
  private ColorMatcher colorMatcher;

  private JFrame frame;

  private JFileChooser fileChooser;

  /**
   * This method starts the {@link ColorMatcherExample} demo.
   * 
   * @param args
   *          command line arguments. Not used.
   */
  public static void main(String[] args) {
    ColorMatcherExample tmpColorMatcher = new ColorMatcherExample();
    try {
      tmpColorMatcher.initialize();
      tmpColorMatcher.run();
    } finally {
      tmpColorMatcher.shutdown();
    }
  }

  private void shutdown() {
    colorMatcher.shutdown();
  }

  private void run() {
    generateNewColors();
  }

  private void initializeComponents() {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
      getLogger().warn("Could not set Look And Feel.", e);
    }

    frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("ColorMatcher");

    fileChooser = new JFileChooser(".");

    Container tmpContentPane = frame.getContentPane();
    tmpContentPane.setLayout(new MigLayout("", "", ""));

    JTextArea tmpIntro = new JTextArea(
	"How well do these colors fit together? Judge them via the slider"
	    + " (0 = bad, 100 = good match), then click on 'Add' to generate a new "
	    + "pair. The initial value of the slider shows what the ColorMatcher "
	    + "currently thinks how well the colors fit.");
    tmpIntro.setLineWrap(true);
    tmpIntro.setEditable(false);
    tmpIntro.setOpaque(false);
    tmpContentPane.add(tmpIntro, "span, growx");

    color1Panel = new JPanel();
    color1Panel.setMinimumSize(new Dimension(200, 200));
    tmpContentPane.add(color1Panel, "");

    color2Panel = new JPanel();
    color2Panel.setMinimumSize(new Dimension(200, 200));
    tmpContentPane.add(color2Panel, "wrap");

    matchSlider = new JSlider(0, 100);
    tmpContentPane.add(matchSlider, "wrap");

    JButton tmpJButton = new JButton("Add");
    tmpJButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent aE) {
	saveMatchingAndGenerateNewColors();
      }

    });
    tmpContentPane.add(tmpJButton);

    JButton tmpSaveButton = new JButton("Save");
    tmpSaveButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent anEvent) {
	handleSaveButtonClick(anEvent);
      }

    });
    tmpContentPane.add(tmpSaveButton);

    JButton tmpLoadButton = new JButton("Load");
    tmpLoadButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent anEvent) {
	handleLoadButtonClick(anEvent);
      }

    });
    tmpContentPane.add(tmpLoadButton);

    frame.setSize(600, 600);
    frame.setVisible(true);
  }

  protected void handleSaveButtonClick(ActionEvent aEvent) {
    int tmpDialogResult = fileChooser.showSaveDialog(frame);
    if (tmpDialogResult == JFileChooser.APPROVE_OPTION) {
      File tmpSelectedFile = fileChooser.getSelectedFile();
      saveNetworkAsFile(tmpSelectedFile);
    }
  }

  protected void handleLoadButtonClick(ActionEvent aEvent) {
    int tmpDialogResult = fileChooser.showOpenDialog(frame);
    if (tmpDialogResult == JFileChooser.APPROVE_OPTION) {
      File tmpSelectedFile = fileChooser.getSelectedFile();
      loadNetworkFromFile(tmpSelectedFile);
    }
  }

  protected void saveMatchingAndGenerateNewColors() {
    CustomColor tmpColor1 = new CustomColor(color1Panel.getBackground().getRGB());
    CustomColor tmpColor2 = new CustomColor(color2Panel.getBackground().getRGB());
    double tmpMatch = matchSlider.getValue() / 100d;

    colorMatcher.addPairAndTrainNetwork(tmpColor1, tmpColor2, tmpMatch);
    generateNewColors();
  }

  protected void saveNetworkAsFile(File aSelectedFile) {
    try (OutputStream tmpOutputStream = new FileOutputStream(aSelectedFile)) {
      colorMatcher.saveNetworkToStream(tmpOutputStream);
    } catch (NetworkPersistenceError | IOException e) {
      getLogger().error(e.getMessage(), e);
    }
  }

  protected void loadNetworkFromFile(File aSelectedFile) {
    try (InputStream tmpInputStream = new FileInputStream(aSelectedFile)) {
      colorMatcher.loadNetworkFromStream(tmpInputStream);
      refreshPrediction();
    } catch (NetworkPersistenceError | IOException e) {
      getLogger().error(e.getMessage(), e);
    }
  }

  private void refreshPrediction() {
    Color tmpNewColor1 = color1Panel.getBackground();
    Color tmpNewColor2 = color2Panel.getBackground();

    predictAndUpdateOutput(tmpNewColor1, tmpNewColor2);
  }

  private void generateNewColors() {
    Color tmpNewColor1 = getRandomColor();
    color1Panel.setBackground(tmpNewColor1);
    Color tmpNewColor2 = getRandomColor();
    color2Panel.setBackground(tmpNewColor2);

    predictAndUpdateOutput(tmpNewColor1, tmpNewColor2);
  }

  private void predictAndUpdateOutput(Color aColor1, Color aColor2) {
    double tmpData = colorMatcher.computeMatch(new CustomColor(aColor1.getRGB()), new CustomColor(aColor2.getRGB()));
    String tmpMessage = "Predicted match: "
	+ tmpData + " " + aColor1 + " " + aColor2;
    getLogger().info(tmpMessage);
    matchSlider.setValue((int) (tmpData * 100));
  }

  private Color getRandomColor() {
    Random tmpRandom = new Random();
    int tmpR = tmpRandom.nextInt(255);
    int tmpG = tmpRandom.nextInt(255);
    int tmpB = tmpRandom.nextInt(255);
    return new Color(tmpR, tmpG, tmpB);
  }

  private void initialize() {
    colorMatcher = new ColorMatcher();
    initializeComponents();
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }
}