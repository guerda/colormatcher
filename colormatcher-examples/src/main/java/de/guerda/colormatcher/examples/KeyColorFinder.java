package de.guerda.colormatcher.examples;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import net.miginfocom.swing.MigLayout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;

public class KeyColorFinder {

  private static final int QUALITY = 5;
  private static final int NUMBER_OF_COLORS = 9;

  public static void main(String[] args) throws Exception {
    KeyColorFinder tmpKeyColorFinder = new KeyColorFinder();

    JFileChooser tmpJFileChooser = new JFileChooser();
    tmpJFileChooser.setFileFilter(new FileFilter() {

      @Override
      public String getDescription() {
	return "Images (*.jpg, *.png)";
      }

      @Override
      public boolean accept(File aF) {
	if (aF.isDirectory()) {
	  return true;
	}
	if (aF.isFile()) {
	  String tmpLowerCase = aF.getName().toLowerCase();
	  if (tmpLowerCase.endsWith(".jpg") || tmpLowerCase.endsWith(".jpeg") || tmpLowerCase.endsWith(".png")
	      || tmpLowerCase.endsWith(".bmp")) {
	    return true;
	  }
	}
	return false;
      }
    });
    int tmpShowOpenDialog = tmpJFileChooser.showOpenDialog(null);
    if (JFileChooser.APPROVE_OPTION == tmpShowOpenDialog) {
      File tmpImageFile = tmpJFileChooser.getSelectedFile();
      tmpKeyColorFinder.run(tmpImageFile);
    }
  }

  private Logger logger;

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  private void run(File anImageFile) throws Exception {
    // Read image from file
    getLogger().info("Read image from file...");
    BufferedImage tmpImage = ImageIO.read(anImageFile);

    // Convert image to pixel array
    getLogger().info("Convert image to pixel array...");
    int w = tmpImage.getWidth();
    int h = tmpImage.getHeight();

    // Convert to ML Data set for clustering
    getLogger().info("Convert to Color list Median Cut Color Quantization...");
    List<Color> tmpColorList = new ArrayList<>();
    int[] tmpPixels = tmpImage.getRGB(0, 0, w, h, null, 0, w);
    for (int tmpPixel : tmpPixels) {
      Color tmpColor = new Color(tmpPixel);
      tmpColorList.add(tmpColor);
    }

    // Perform Median Cut Color Quantization
    getLogger().info("Perform Median Cut Color Quantization...");
    ImageMCCQ tmpColorQuantization = new ImageMCCQ();
    List<CustomColor> tmpDominantColors = tmpColorQuantization.getDominantColors(tmpImage, NUMBER_OF_COLORS, QUALITY);

    JFrame tmpJFrame = new JFrame();
    tmpJFrame.setSize(300 + NUMBER_OF_COLORS * 40, 350);
    tmpJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    tmpJFrame.setLayout(new MigLayout());

    JLabel tmpImageCanvas = new JLabel();
    Image tmpScaledImage = getScaledImage(tmpImage, 300, 300);
    tmpImageCanvas.setIcon(new ImageIcon(tmpScaledImage));

    tmpJFrame.getContentPane().add(tmpImageCanvas, "span 5");

    for (CustomColor tmpColor : tmpDominantColors) {
      JPanel tmpJPanel = new JPanel();
      tmpJPanel.setSize(150, 150);
      tmpJPanel.setPreferredSize(new Dimension(150, 150));
      tmpJPanel.setBackground(new Color(tmpColor.getRed(), tmpColor.getGreen(), tmpColor.getBlue()));
      tmpJFrame.getContentPane().add(tmpJPanel);
      getLogger().info("Dominant Color: " + tmpColor);
    }
    tmpJFrame.setVisible(true);

    getLogger().info("Done.");

  }

  private Image getScaledImage(BufferedImage srcImg, int w, int h) {
    return ImageUtil.getScaledImage(srcImg, w, h);
  }

}
