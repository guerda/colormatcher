package de.guerda.colormatcher.examples;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.TransferHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageFileDropHandler extends TransferHandler {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private DragAndDropExtractor dragAndDropExtractor;
  private Logger logger;

  public ImageFileDropHandler(DragAndDropExtractor aDragAndDropExtractor) {
    super();
    dragAndDropExtractor = aDragAndDropExtractor;
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  @Override
  public boolean canImport(TransferSupport aSupport) {
    return true;
  }

  @Override
  public boolean importData(TransferSupport aSupport) {
    getLogger().debug("Start importing data...");
    if (!canImport(aSupport)) {
      return false;
    }

    if (!aSupport.isDataFlavorSupported(DataFlavor.javaFileListFlavor) && aSupport.isDataFlavorSupported(DataFlavor.imageFlavor)) {
      return false;
    }
    getLogger().debug("Extract image from Transferable");
    Transferable tmpTransferable = aSupport.getTransferable();
    try {
      List<?> tmpFileList = (List<?>) tmpTransferable.getTransferData(DataFlavor.javaFileListFlavor);
      for (Object tmpFile : tmpFileList) {
        BufferedImage tmpRead = ImageIO.read((File) tmpFile);
	getLogger().debug("Read image: " + tmpRead.getWidth() + "x" + tmpRead.getHeight());
	dragAndDropExtractor.setImage(tmpRead);
      }
    } catch (UnsupportedFlavorException | IOException e) {
      getLogger().error("Could not transfer image from DnD or Clipboard", e);
    }
    return true;
  }

}