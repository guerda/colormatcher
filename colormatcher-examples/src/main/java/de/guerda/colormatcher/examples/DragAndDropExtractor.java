package de.guerda.colormatcher.examples;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;
import de.guerda.colormatcher.examples.webcam.ColorPalette;
import net.miginfocom.swing.MigLayout;

public class DragAndDropExtractor extends JFrame {

  /**
   * Default serial version UID.
   */
  private static final long serialVersionUID = 1L;

  public static void main(String[] args) {
    DragAndDropExtractor tmpDragAndDropExtractor = new DragAndDropExtractor();
    tmpDragAndDropExtractor.initialize();
    tmpDragAndDropExtractor.run();
  }

  private JLabel dropLabel;
  private Logger logger;
  private ImageMCCQ imageMCCQ;
  private ColorPalette colorPalette;

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  private void initialize() {
    imageMCCQ = new ImageMCCQ();

    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
      getLogger().warn("Could not set Look And Feel.", e);
    }

    setLayout(new MigLayout());
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    dropLabel = new JLabel("Drop image here for color extraction");
    dropLabel.setMinimumSize(new Dimension(600, 600));
    dropLabel.setPreferredSize(new Dimension(600, 600));
    dropLabel.setTransferHandler(new ImageFileDropHandler(this));
    getContentPane().add(dropLabel, "wrap");

    colorPalette = new ColorPalette();
    colorPalette.setColorBoxSize(80);
    getContentPane().add(colorPalette);
    ArrayList<CustomColor> tmpArrayList = new ArrayList<>();
    tmpArrayList.add(new CustomColor(Color.lightGray.getRGB()));
    colorPalette.showColors(tmpArrayList);
    pack();
  }

  public void setImage(BufferedImage anImage) {
    if (anImage == null) {
      return;
    }
    Image tmpScaledImage = ImageUtil.getScaledImage(anImage, 600, 600);
    dropLabel.setIcon(new ImageIcon(tmpScaledImage));

    List<CustomColor> tmpDominantColors = imageMCCQ.getDominantColors(anImage, 5);
    colorPalette.showColors(tmpDominantColors);
  }

  private void run() {
    setVisible(true);
  }

}
