package de.guerda.colormatcher.examples;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import de.guerda.colormatcher.core.extraction.MedianCutColorQuantization;

public class ImageMCCQTest {

  private static final String IMAGE_BMP = "lena_secret.bmp";

  private ImageMCCQ imageMCCQ;

  @Before
  public void setup() {
    imageMCCQ = new ImageMCCQ();
  }

  private BufferedImage loadImageFromResource(String aFileName) throws IOException {
    InputStream tmpInputStream = getClass().getClassLoader().getResourceAsStream(aFileName);
    BufferedImage tmpImage = ImageIO.read(tmpInputStream);
    return tmpImage;
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetDominantColorsCheckArgumentImageNull() {
    imageMCCQ.getDominantColors((BufferedImage) null, 2, 1);
    // (BufferedImage anImage, int aCountOfColors, int aQuality)
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetDominantColorsCheckArgumentCountOfColorsZero() throws Exception {
    BufferedImage tmpImage = loadImageFromResource(IMAGE_BMP);
    imageMCCQ.getDominantColors(tmpImage, 0, 1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetDominantColorsCheckArgumentCountOfColorsLargerThanCountOfPixels() throws Exception {
    BufferedImage tmpImage = loadImageFromResource(IMAGE_BMP);
    int tmpCountOfColors = tmpImage.getWidth() * tmpImage.getHeight() + 1;
    imageMCCQ.getDominantColors(tmpImage, tmpCountOfColors, 1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetDominantColorsCheckArgumentQualityZero() throws Exception {
    BufferedImage tmpImage = loadImageFromResource(IMAGE_BMP);
    imageMCCQ.getDominantColors(tmpImage, 2, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetDominantColorsCheckArgumentQualityTooHigh() throws Exception {
    BufferedImage tmpImage = loadImageFromResource(IMAGE_BMP);
    imageMCCQ.getDominantColors(tmpImage, 2, MedianCutColorQuantization.MAX_QUALITY + 1);
  }

}
