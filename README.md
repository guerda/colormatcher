Colormatcher
=

Colormatcher is a library to extract colors out of an image and score the match between each two of them with the help of a neural network.


Usage
-
You can find examples in the colormatcher-examples submodule.

License
-
[GNU General Public License, Version 3](http://www.gnu.org/licenses/gpl.html)
