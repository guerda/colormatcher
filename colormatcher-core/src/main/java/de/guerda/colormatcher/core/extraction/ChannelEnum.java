package de.guerda.colormatcher.core.extraction;

/**
 * This enum describes the three channels in RGB images. It contains the values
 * {@link #RED}, {@link #GREEN} and {@link #BLUE}.
 * 
 * @author Philip guerda Gillißen
 *
 */
public enum ChannelEnum {

  /**
   * This value contains the red channel.
   */
  RED,

  /**
   * This value contains the green channel.
   */
  GREEN,

  /**
   * This value contains the blue channel.
   */
  BLUE;

}
