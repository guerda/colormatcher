package de.guerda.colormatcher.core;

public class CustomColor {

  private int red;
  private int green;
  private int blue;

  public CustomColor(int aRed, int aGreen, int aBlue) {
    super();
    red = aRed;
    green = aGreen;
    blue = aBlue;
  }

  public CustomColor(int aRgb) {
    red = (aRgb >> 16) & 0xFF;
    green = (aRgb >> 8) & 0xFF;
    blue = (aRgb >> 0) & 0xFF;
  }

  /**
   * Simple getter for {@link red}
   * 
   * @return the red
   */
  public int getRed() {
    return red;
  }

  /**
   * Simple setter for {@link red}
   * 
   * @param aRed
   *          the red to set
   */
  public void setRed(int aRed) {
    red = aRed;
  }

  /**
   * Simple getter for {@link green}
   * 
   * @return the green
   */
  public int getGreen() {
    return green;
  }

  /**
   * Simple setter for {@link green}
   * 
   * @param aGreen
   *          the green to set
   */
  public void setGreen(int aGreen) {
    green = aGreen;
  }

  /**
   * Simple getter for {@link blue}
   * 
   * @return the blue
   */
  public int getBlue() {
    return blue;
  }

  /**
   * Simple setter for {@link blue}
   * 
   * @param aBlue
   *          the blue to set
   */
  public void setBlue(int aBlue) {
    blue = aBlue;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "CustomColor [red=" + red + ", green=" + green + ", blue=" + blue + "]";
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    CustomColor other = (CustomColor) obj;
    if (blue != other.blue) {
      return false;
    }
    if (green != other.green) {
      return false;
    }
    if (red != other.red) {
      return false;
    }
    return true;
  }

}
