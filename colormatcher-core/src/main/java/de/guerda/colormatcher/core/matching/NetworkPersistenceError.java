package de.guerda.colormatcher.core.matching;


/**
 * This exception indicates that the neural network couldn't be saved. This can
 * be caused by IOErrors within the library or something similar. The real
 * reason is hidden in the cause parameter.
 * 
 * @author Philip guerda Gillißen
 *
 */
public final class NetworkPersistenceError extends Exception {

  /**
   * Default serial version uid.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Creates a {@link NetworkPersistenceError} to indicate that the neural
   * network couldn't be saved. The real reason is hidden in the cause
   * parameter.
   * 
   * @param aMessage
   *          - the message what happened, to be displayed to the user.
   * @param aCause
   *          - the real cause what caused that error.
   */
  public NetworkPersistenceError(final String aMessage,
      final Exception aCause) {
    super(aMessage, aCause);
  }
}
