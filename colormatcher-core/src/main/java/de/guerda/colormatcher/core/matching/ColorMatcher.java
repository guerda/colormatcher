package de.guerda.colormatcher.core.matching;

import java.io.InputStream;
import java.io.OutputStream;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.guerda.colormatcher.core.CustomColor;

/**
 * This class is able to match colors against each other, based on a training
 * set. The matching works with a neural network.
 * 
 * @author Philip "guerda" Gillißen
 *
 */
public final class ColorMatcher {

  /**
   * This constant defines the minimum error to allow during training.
   */
  private static final double MINIMUM_ERROR = 0.01;

  /**
   * This constant defines, how many input values a data set has. 3 color
   * channels times 2 equals 6.
   */
  private static final int SIZE_OF_DATASET = 6;

  /**
   * This constant defines the maximum color range for a color channel.
   */
  private static final double MAX_COLOR_RANGE = 255d;

  /**
   * This constant defines the count of input neurons in the network. Two colors
   * à three color channels results in six input neurons.
   */
  private static final int NEURON_COUNT_INPUT = 6;

  /**
   * This constant defines the count of output neurons in the network.
   */
  private static final int NEURON_COUNT_HIDDEN = 12;

  /**
   * This constant defines the count of output neurons in the network.
   */
  private static final int NEURON_COUNT_OUTPUT = 1;

  /**
   * This member contains the matching network.
   */
  private BasicNetwork network;

  /**
   * This member contains the class specific logger.
   */
  private Logger logger;

  /**
   * This member contains the training data.
   */
  private MLDataSet training;

  /**
   * This constructor creates a color matcher and initializes the neural
   * network.
   */
  public ColorMatcher() {
    network = new BasicNetwork();
    // Input layer, 6 values, 2xRGB
    network.addLayer(new BasicLayer(null, true, NEURON_COUNT_INPUT));
    // Hidden layer, 12 values
    ActivationSigmoid tmpActSig = new ActivationSigmoid();
    BasicLayer tmpHiddenLayer =
	new BasicLayer(tmpActSig, true, NEURON_COUNT_HIDDEN);
    network.addLayer(tmpHiddenLayer);

    // Output layer
    BasicLayer tmpOutput =
	new BasicLayer(tmpActSig, false, NEURON_COUNT_OUTPUT);
    network.addLayer(tmpOutput);
    network.getStructure().finalizeStructure();
    network.reset();

    training = new BasicMLDataSet();
  }

  /**
   * Converts a pair of colors to a machine learning data set.
   * 
   * @param aColor1
   *          the first color
   * @param aColor2
   *          the second color
   * @return a machine learning data set with both colors
   */
  protected MLData getColorsAsMLData(final CustomColor aColor1, final CustomColor aColor2) {
    MLData tmpResult = new BasicMLData(SIZE_OF_DATASET);
    int i = 0;
    tmpResult.add(i++, aColor1.getRed() / MAX_COLOR_RANGE);
    tmpResult.add(i++, aColor1.getGreen() / MAX_COLOR_RANGE);
    tmpResult.add(i++, aColor1.getBlue() / MAX_COLOR_RANGE);
    tmpResult.add(i++, aColor2.getRed() / MAX_COLOR_RANGE);
    tmpResult.add(i++, aColor2.getGreen() / MAX_COLOR_RANGE);
    tmpResult.add(i++, aColor2.getBlue() / MAX_COLOR_RANGE);
    return tmpResult;
  }

  /**
   * Save the trained network to an outputstream for persistence.
   * 
   * @param anOutputStream
   *          the stream to write the network to
   * @throws NetworkPersistenceError
   *           if the network couldn't be saved
   */
  public void saveNetworkToStream(final OutputStream anOutputStream)
      throws NetworkPersistenceError {
    if (anOutputStream == null) {
      throw new IllegalArgumentException("Outputstream was null."
          + "Please provide a valid output stream!");
    }
    EncogDirectoryPersistence.saveObject(anOutputStream, network);
  }

  /**
   * This method loads a trained network from an input stream. All weights are
   * read in and the network is initialized with these weights.
   * 
   * @param anInputStream
   *          the input source for the network weights
   * @throws NetworkPersistenceError
   *           if the network couldn't be read.
   */
  public void loadNetworkFromStream(final InputStream anInputStream)
      throws NetworkPersistenceError {
    if (anInputStream == null) {
      throw new IllegalArgumentException("Inputstream was null."
	  + "Please provide a valid output stream!");
    }
    Object tmpLoadObject = EncogDirectoryPersistence.loadObject(anInputStream);
    network = (BasicNetwork) tmpLoadObject;

  }

  /**
   * This method computes the match of two colors, according to the neural
   * network. The match is expressed as percentage (0-1).
   * 
   * @param aColor1
   *          the first color to match
   * @param aColor2
   *          the second color to match
   * @return the match, expressed as a value between 0 and 1.
   */
  public double computeMatch(final CustomColor aColor1, final CustomColor aColor2) {
    MLData tmpInput = getColorsAsMLData(aColor1, aColor2);
    MLData tmpCompute = network.compute(tmpInput);
    return tmpCompute.getData(0);
  }

  /**
   * This method trains the network with a training set. Used internally to
   * retrain.
   * 
   * @param aTraining
   *          the training set to use.
   */
  // TODO: test this method!
  protected void trainNetwork(final MLDataSet aTraining) {
    if (aTraining.size() > 0) {
      ResilientPropagation tmpProp =
	  new ResilientPropagation(network, aTraining);
      int tmpEpoch = 1;
      do {
	tmpProp.iteration();
	String tmpMessage = "Training epoch #" + tmpEpoch
	    + " " + tmpProp.getError();
	getLogger().debug(tmpMessage);
	tmpEpoch++;
      } while (tmpProp.getError() > MINIMUM_ERROR);
      tmpProp.finishTraining();
    }
  }

  /**
   * Creates and returns a class specific logger.
   *
   * @return the logger
   */
  public Logger getLogger() {
    if (logger == null) {
      logger = LoggerFactory.getLogger(getClass());
    }
    return logger;
  }

  /**
   * This method adds a match of two colors and a match to the training sets and
   * retrains the network.
   * 
   * @param aColor1
   *          the first color
   * @param aColor2
   *          the second color
   * @param aMatch
   *          how much they both match (0-1)
   */
  public void addPairAndTrainNetwork(final CustomColor aColor1, final CustomColor aColor2,
      final double aMatch) {
    if (aColor1 == null) {
      throw new IllegalArgumentException("First color was null."
	  + "Please provide a valid first color!");
    }
    if (aColor2 == null) {
      throw new IllegalArgumentException("Second color was null."
	  + "Please provide a valid second color!");
    }
    if (aMatch < 0 || aMatch > 1) {
      throw new IllegalArgumentException("Provided match was not valid."
	  + "Please provide a match between 0 and 1!");
    }
    getLogger().info(aMatch + " " + aColor1 + " & " + aColor2);
    MLData tmpInputData = getColorsAsMLData(aColor1, aColor2);
    BasicMLData tmpIdealData = new BasicMLData(new double[] { aMatch });
    training.add(tmpInputData, tmpIdealData);
    trainNetwork(training);
  }

  /**
   * Shuts down the color matcher and stops the neural network engine. Should be
   * called on tear down, but I'm no cop.
   */
  public void shutdown() {
    Encog.getInstance().shutdown();
  }

}