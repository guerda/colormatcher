package de.guerda.colormatcher.core.extraction;

import java.util.Comparator;

import de.guerda.colormatcher.core.CustomColor;

/**
 * This class implements a {@link Comparator} to compare {@link CustomColor}s.
 * The comparator must be created with a sorting channel (specified by
 * {@link ChannelEnum}). Then the given colors are sorted via this channel.
 * 
 * @author Philip guerda Gillißen
 *
 */
public final class CustomColorComparator implements Comparator<CustomColor> {

  /**
   * This member contains the color channel to sort by.
   */
  private ChannelEnum sortingChannel;

  /**
   * Creates a {@link CustomColorComparator} which sorts colors via a specified
   * channel.
   * 
   * @param aSortingChannel
   *          - the channel to sort via
   */
  public CustomColorComparator(final ChannelEnum aSortingChannel) {
    if (aSortingChannel == null) {
      String tmpMessage = "Given sorting channel was null."
        + "Please provide a valid sorting channel!";
      throw new IllegalArgumentException(tmpMessage);
    }
    sortingChannel = aSortingChannel;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(final CustomColor aColor1, final CustomColor aColor2) {
    if (ChannelEnum.RED == sortingChannel) {
      return aColor2.getRed() - aColor1.getRed();
    } else if (ChannelEnum.GREEN == sortingChannel) {
      return aColor2.getGreen() - aColor1.getGreen();
    } else {
      return aColor2.getBlue() - aColor1.getBlue();
    }
  }
}
