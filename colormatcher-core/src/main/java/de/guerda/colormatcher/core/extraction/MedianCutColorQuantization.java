package de.guerda.colormatcher.core.extraction;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.math.IntMath;

import de.guerda.colormatcher.core.CustomColor;

/**
 * This class helps you to extract dominant colors from an image. The extraction
 * is performed via the so called Median Cut. You can control the quality/speed
 * balance via the quality parameter. More on this quantization: <a
 * href="https://en.wikipedia.org/wiki/Median_cut">Wikipedia on Median Cut</a>.
 * 
 * @author Philip "guerda" Gillißen
 *
 */
public class MedianCutColorQuantization {

  /**
   * This constant defines the default quality for the color extraction. This
   * means that every 50th pixel is accounted for the color extraction.
   */
  public static final int DEFAULT_QUALITY = 50;

  /**
   * This constant defines the maximum quality for the median cut algorithm. A
   * quality of 100 means that every pixel is accounted.
   */
  public static final int MAX_QUALITY = 100;

  /**
   * Determines n dominant colors from a list of given colors.
   * 
   * @param aColorList
   *          the list of colors to inspect
   * @param aCountOfColors
   *          the count of colors. Should be a 2^n (e.g. 2, 4, 8, 16 etc).
   * 
   * @return a list of dominant colors
   */
  public List<CustomColor> getDominantColors(
      final ArrayList<CustomColor> aColorList, final int aCountOfColors) {
    return getDominantColors(aColorList, aCountOfColors, DEFAULT_QUALITY);
  }

  /**
   * Determines n dominant colors from a list of given colors.
   * 
   * @param aColorList
   *          the list of colors to inspect
   * @param aCountOfColors
   *          the count of colors. Should be a 2^n (e.g. 2, 4, 8, 16 etc).
   * @param aQuality
   *          an indicator of quality of the determination. Ranges from 1-
   *          {@link #MAX_QUALITY}.
   * 
   * @return a list of dominant colors
   */
  public List<CustomColor> getDominantColors(
      final List<CustomColor> aColorList, final int aCountOfColors,
      final int aQuality) {

    if (aCountOfColors < 1) {
      throw new IllegalArgumentException("Requested count of colors"
	  + "was zero or negative. Please provide a positive count of "
	  + "colors!");
    }

    if (aQuality < 1) {
      throw new IllegalArgumentException("Requested quality was zero or "
	  + "negative. Please provide a positive quality (1-" + MAX_QUALITY
	  + "!");
    }

    if (aQuality > MAX_QUALITY) {
      throw new IllegalArgumentException("Requested quality was zero or"
	  + " negative. Please provide a positive quality (1-" + MAX_QUALITY
	  + ")!");
    }

    // Reduce colors based on the quality
    ArrayList<CustomColor> tmpReducedColors = new ArrayList<>();
    int tmpQualityStep = MAX_QUALITY - DEFAULT_QUALITY;
    for (int i = 0; i < aColorList.size(); i = i + tmpQualityStep) {
      tmpReducedColors.add(aColorList.get(i));
    }

    ArrayList<CustomColor> tmpResult = new ArrayList<CustomColor>();
    List<List<CustomColor>> tmpDominantColorBuckets =
	getDominantColorBuckets(tmpReducedColors, aCountOfColors);

    // Average each bucket
    for (List<CustomColor> tmpList : tmpDominantColorBuckets) {
      CustomColor tmpAveragedColor = averageColorList(tmpList);
      tmpResult.add(tmpAveragedColor);
    }
    return tmpResult;
  }

  /**
   * This method determines the channel with the broadest spectrum. This equals
   * with a high dynamic in this channel.
   * 
   * 
   * @param aColorList
   *          the list of colors to determine the most dynamic in
   * @return the {@link ChannelEnum} referring to the most dynamic color
   *         channel.
   */
  protected ChannelEnum findBroadestSpectrum(
      final List<CustomColor> aColorList) {
    int tmpMaxR = Integer.MIN_VALUE;
    int tmpMaxG = Integer.MIN_VALUE;
    int tmpMaxB = Integer.MIN_VALUE;
    int tmpMinR = Integer.MAX_VALUE;
    int tmpMinG = Integer.MAX_VALUE;
    int tmpMinB = Integer.MAX_VALUE;

    for (CustomColor tmpColor : aColorList) {
      tmpMaxR = Math.max(tmpMaxR, tmpColor.getRed());
      tmpMaxG = Math.max(tmpMaxG, tmpColor.getGreen());
      tmpMaxB = Math.max(tmpMaxB, tmpColor.getBlue());
      tmpMinR = Math.min(tmpMinR, tmpColor.getRed());
      tmpMinG = Math.min(tmpMinG, tmpColor.getGreen());
      tmpMinB = Math.min(tmpMinB, tmpColor.getBlue());
    }

    int tmpDiffR = tmpMaxR - tmpMinR;
    int tmpDiffG = tmpMaxG - tmpMinG;
    int tmpDiffB = tmpMaxB - tmpMinB;

    ChannelEnum tmpSortingChannel =
	determineLargestDiff(tmpDiffR, tmpDiffG, tmpDiffB);

    return tmpSortingChannel;
  }

  /**
   * Sorts the given list of colors according to the most dynamic channel
   * (broadest spectrum) and returns a list of buckets. This method calls itself
   * recursively.
   * 
   * @param aColors
   *          list of colors to sort and split into buckets
   * @param aCountOfBuckets
   *          the requested number of buckets
   * @return a list of buckets (bucket = list of color)
   */
  protected List<List<CustomColor>> getDominantColorBuckets(
      final List<CustomColor> aColors, final int aCountOfBuckets) {
    if (aCountOfBuckets < 1) {
      throw new IllegalArgumentException("Requested count of buckets was"
	  + " zero or negative. Please provide a postive count of buckets!");
    }

    ArrayList<List<CustomColor>> tmpResultingColors = new ArrayList<>();
    if (aCountOfBuckets == 1) {
      tmpResultingColors.add(aColors);
      return tmpResultingColors;
    }
    ChannelEnum tmpSortingChannel = findBroadestSpectrum(aColors);
    // Default sorting behavior
    if (tmpSortingChannel == null) {
      tmpSortingChannel = ChannelEnum.RED;
    }

    CustomColorComparator tmpColorComparator = new CustomColorComparator(
	tmpSortingChannel);
    Collections.sort(aColors, tmpColorComparator);

    List<List<CustomColor>> tmpColorBuckets = splitListInHalf(aColors);

    for (List<CustomColor> tmpColorBucket : tmpColorBuckets) {
      List<List<CustomColor>> tmpDominantColorBuckets =
	  getDominantColorBuckets(tmpColorBucket, aCountOfBuckets / 2);
      if (tmpDominantColorBuckets != null) {
	tmpResultingColors.addAll(tmpDominantColorBuckets);
      }
    }

    return tmpResultingColors;
  }

  /**
   * This function splits the given list of colors in two parts. The first list
   * may be one item larger than the second.
   * 
   * @param aColorList
   *          the list of colors to be split into
   * @return a list of two lists of colors.
   */
  protected List<List<CustomColor>> splitListInHalf(
      final List<CustomColor> aColorList) {
    int partitionSize = IntMath.divide(aColorList.size(), 2, RoundingMode.UP);
    List<List<CustomColor>> tmpResult = Lists.partition(
	aColorList, partitionSize);

    return tmpResult;
  }

  /**
   * This method averages all given colors to one color. This is done via
   * averaging all channels.
   * 
   * @param aColorList
   *          the list to be averaged.
   * @return a {@link CustomColor} which is the average of all others.
   */
  protected CustomColor averageColorList(final List<CustomColor> aColorList) {
    int tmpRedSum = 0;
    int tmpGreenSum = 0;
    int tmpBlueSum = 0;

    for (CustomColor tmpColor : aColorList) {
      tmpRedSum += tmpColor.getRed();
      tmpGreenSum += tmpColor.getGreen();
      tmpBlueSum += tmpColor.getBlue();
    }
    int tmpSize = aColorList.size();

    int tmpR = tmpRedSum / tmpSize;
    int tmpG = tmpGreenSum / tmpSize;
    int tmpB = tmpBlueSum / tmpSize;

    return new CustomColor(tmpR, tmpG, tmpB);
  }

  /**
   * This function determines, which channel has the largest diff. Simply a
   * function to determine the correct {@link ChannelEnum}.
   * 
   * @param aDiffR
   *          the difference in channel red.
   * @param aDiffG
   *          the difference in channel green.
   * @param aDiffB
   *          the difference in channel blue.
   * @return the channel enum with the largest diff.
   */
  protected ChannelEnum determineLargestDiff(final int aDiffR,
      final int aDiffG, final int aDiffB) {
    if (aDiffR > aDiffG && aDiffR > aDiffB) {
      return ChannelEnum.RED;
    } else if (aDiffG > aDiffR && aDiffG > aDiffB) {
      return ChannelEnum.GREEN;
    } else if (aDiffB > aDiffR && aDiffB > aDiffG) {
      return ChannelEnum.BLUE;
    }
    return null;
  }
}
