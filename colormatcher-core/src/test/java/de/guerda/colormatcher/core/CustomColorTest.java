package de.guerda.colormatcher.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CustomColorTest {

  @Test
  public void testCreateColorWithRGBValueRed() {
    int tmpValue = 0xFF0000;
    CustomColor tmpCustomColor = new CustomColor(tmpValue);
    assertEquals(255, tmpCustomColor.getRed());
    assertEquals(0, tmpCustomColor.getGreen());
    assertEquals(0, tmpCustomColor.getBlue());
  }

  @Test
  public void testCreateColorWithRGBValueGreen() {
    int tmpValue = 0xFF0000;
    CustomColor tmpCustomColor = new CustomColor(tmpValue);
    assertEquals(255, tmpCustomColor.getRed());
    assertEquals(0, tmpCustomColor.getGreen());
    assertEquals(0, tmpCustomColor.getBlue());
  }

  @Test
  public void testCreateColorWithRGBValueBlue() {
    int tmpValue = 0x00FF00;
    CustomColor tmpCustomColor = new CustomColor(tmpValue);
    assertEquals(0, tmpCustomColor.getRed());
    assertEquals(255, tmpCustomColor.getGreen());
    assertEquals(0, tmpCustomColor.getBlue());
  }

  @Test
  public void testCreateColorWithRGBValueGray() {
    int tmpValue = 0x808080;
    CustomColor tmpCustomColor = new CustomColor(tmpValue);
    assertEquals(128, tmpCustomColor.getRed());
    assertEquals(128, tmpCustomColor.getGreen());
    assertEquals(128, tmpCustomColor.getBlue());
  }

}
