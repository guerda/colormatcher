package de.guerda.colormatcher.core.extraction;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.guerda.colormatcher.core.CustomColor;


//CHECKSTYLE:IGNORE
public final class ColorComparatorTest {

  @Test(expected = IllegalArgumentException.class)
  public void testComparaturNull() {
    new CustomColorComparator(null);
  }

  @Test
  public void testCompareRed() {
    CustomColorComparator tmpColorComparator = new CustomColorComparator(ChannelEnum.RED);
    int tmpCompare = tmpColorComparator.compare(new CustomColor(0xFF0000), new CustomColor(0x000000));
    assertTrue(tmpCompare < 0);
  }

  @Test
  public void testCompareGreen() {
    CustomColorComparator tmpColorComparator = new CustomColorComparator(ChannelEnum.GREEN);
    int tmpCompare = tmpColorComparator.compare(new CustomColor(0x00FF00), new CustomColor(0x000000));
    assertTrue(tmpCompare < 0);
  }

  @Test
  public void testCompareBlue() {
    CustomColorComparator tmpColorComparator = new CustomColorComparator(ChannelEnum.BLUE);
    int tmpCompare = tmpColorComparator.compare(new CustomColor(0x0000FF), new CustomColor(0x000000));
    assertTrue(tmpCompare < 0);
  }

  @Test
  public void testCompareRedSmaller() {
    CustomColorComparator tmpColorComparator = new CustomColorComparator(ChannelEnum.RED);
    int tmpCompare = tmpColorComparator.compare(new CustomColor(0xAA0000), new CustomColor(0xFFFFFF));
    assertTrue(tmpCompare > 0);
  }

  @Test
  public void testCompareGreenSmaller() {
    CustomColorComparator tmpColorComparator = new CustomColorComparator(ChannelEnum.GREEN);
    int tmpCompare = tmpColorComparator.compare(new CustomColor(0x00AA00), new CustomColor(0xFFFFFF));
    assertTrue(tmpCompare > 0);
  }

  @Test
  public void testCompareBlueSmaller() {
    CustomColorComparator tmpColorComparator = new CustomColorComparator(ChannelEnum.BLUE);
    int tmpCompare = tmpColorComparator.compare(new CustomColor(0x0000AA), new CustomColor(0xFFFFFF));
    assertTrue(tmpCompare > 0);
  }

}
