package de.guerda.colormatcher.core.matching;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.encog.ml.data.MLData;
import org.junit.Before;
import org.junit.Test;

import de.guerda.colormatcher.core.CustomColor;

public class ColorMatcherTest {

  private static final double MATCH_GOOD = 0.5D;

  private static final double DOUBLE_PRECISION = 0.001;

  private ColorMatcher colorMatcher;

  @Before
  public void setup() {
    colorMatcher = new ColorMatcher();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAddPairAndTrainNetworkNullColor1() {
    colorMatcher.addPairAndTrainNetwork(null, new CustomColor(0xFF0000), MATCH_GOOD);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAddPairAndTrainNetworkNullColor2() {
    colorMatcher.addPairAndTrainNetwork(new CustomColor(0xFF0000), null, MATCH_GOOD);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAddPairAndTrainNetworkNegativeMatch() {
    colorMatcher.addPairAndTrainNetwork(new CustomColor(0xFF0000), new CustomColor(0x0000FF), -1D);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAddPairAndTrainNetworkMatchLargerOne() {
    colorMatcher.addPairAndTrainNetwork(new CustomColor(0xFF0000), new CustomColor(0x0000FF), 2D);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSaveNetworkToStreamNullStream() throws Exception {
    colorMatcher.saveNetworkToStream(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testLoadNetworkFromStreamNullStream() throws Exception {
    colorMatcher.loadNetworkFromStream(null);
  }

  @Test
  public void testSaveAndLoadNetwork() throws Exception {
    double tmpFirstMatch = colorMatcher.computeMatch(new CustomColor(0xFF0000), new CustomColor(0x0000FF));
    ByteArrayOutputStream tmpOutputStream = new ByteArrayOutputStream();
    colorMatcher.saveNetworkToStream(tmpOutputStream);
    colorMatcher = new ColorMatcher();
    byte[] tmpBytes = tmpOutputStream.toByteArray();
    ByteArrayInputStream tmpInputStream = new ByteArrayInputStream(tmpBytes);
    colorMatcher.loadNetworkFromStream(tmpInputStream);
    double tmpSecondMatch = colorMatcher.computeMatch(new CustomColor(0xFF0000), new CustomColor(0x0000FF));
    assertEquals(tmpFirstMatch, tmpSecondMatch, DOUBLE_PRECISION);
  }

  @Test
  public void testGetColorsAsMLDataWithRedAndBlue() {
    MLData tmpColorsAsMLData =
	colorMatcher.getColorsAsMLData(new CustomColor(0xFF0000), new CustomColor(0x0000FF));
    assertNotNull(tmpColorsAsMLData);
    assertEquals(1, tmpColorsAsMLData.getData(0), DOUBLE_PRECISION);
    assertEquals(0, tmpColorsAsMLData.getData(1), DOUBLE_PRECISION);
    assertEquals(0, tmpColorsAsMLData.getData(2), DOUBLE_PRECISION);

    assertEquals(0, tmpColorsAsMLData.getData(3), DOUBLE_PRECISION);
    assertEquals(0, tmpColorsAsMLData.getData(4), DOUBLE_PRECISION);
    assertEquals(1, tmpColorsAsMLData.getData(5), DOUBLE_PRECISION);
  }

  @Test
  public void testGetColorsAsMLDataWithMixedColors() {
    CustomColor tmpColor1 = new CustomColor(110, 120, 130);
    CustomColor tmpColor2 = new CustomColor(60, 70, 80);
    MLData tmpColorsAsMLData =
	colorMatcher.getColorsAsMLData(tmpColor1, tmpColor2);
    assertNotNull(tmpColorsAsMLData);
    assertEquals(0.431, tmpColorsAsMLData.getData(0), DOUBLE_PRECISION);
    assertEquals(0.470, tmpColorsAsMLData.getData(1), DOUBLE_PRECISION);
    assertEquals(0.509, tmpColorsAsMLData.getData(2), DOUBLE_PRECISION);

    assertEquals(0.235, tmpColorsAsMLData.getData(3), DOUBLE_PRECISION);
    assertEquals(0.274, tmpColorsAsMLData.getData(4), DOUBLE_PRECISION);
    assertEquals(0.313, tmpColorsAsMLData.getData(5), DOUBLE_PRECISION);
  }

}
