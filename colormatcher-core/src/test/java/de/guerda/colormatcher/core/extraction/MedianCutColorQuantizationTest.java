package de.guerda.colormatcher.core.extraction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import de.guerda.colormatcher.core.CustomColor;

// CHECKSTYLE:OFF
public final class MedianCutColorQuantizationTest {

  private static final String IMAGE_PIXEL_RED = "pixel_red.png";
  private static final String IMAGE_BMP = "lena_secret.bmp";
  private static final String IMAGE_RANGE_RED = "range_red.png";
  private static final String IMAGE_RANGE_GREEN = "range_green.png";
  private static final String IMAGE_RANGE_BLUE = "range_blue.png";
  private static final String IMAGE_JPG = "photo1.jpg";
  private MedianCutColorQuantization medianCutColorQuantization;

  @Before
  public void setup() {
    medianCutColorQuantization = new MedianCutColorQuantization();
  }

  @Test
  public void testFindBroadestSpectrum() throws Exception {
    ArrayList<CustomColor> tmpColors = new ArrayList<>();
    tmpColors.add(new CustomColor(0xFF0000));
    tmpColors.add(new CustomColor(0, 0, 10));
    ChannelEnum tmpEnum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertNotNull(tmpEnum);
    assertEquals(ChannelEnum.RED, tmpEnum);
  }

  @Test
  public void testFindBroadestSpectrumGreen() throws Exception {
    ArrayList<CustomColor> tmpColors = new ArrayList<>();
    tmpColors.add(new CustomColor(0x00FF00));
    tmpColors.add(new CustomColor(0, 0, 10));
    ChannelEnum tmpEnum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertNotNull(tmpEnum);
    assertEquals(ChannelEnum.GREEN, tmpEnum);
  }

  @Test
  public void testFindBroadestSpectrumBlue() throws Exception {
    ArrayList<CustomColor> tmpColors = new ArrayList<>();
    tmpColors.add(new CustomColor(0x0000FF));
    tmpColors.add(new CustomColor(0, 0, 10));
    ChannelEnum tmpEnum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertNotNull(tmpEnum);
    assertEquals(ChannelEnum.BLUE, tmpEnum);
  }

  @Test
  public void testDetermineLargestDiffAllEqual() {
    ChannelEnum tmpDetermineLargestDiff = medianCutColorQuantization.determineLargestDiff(0, 0, 0);
    assertNull(tmpDetermineLargestDiff);
  }

  @Test
  public void testDetermineLargestFirstLargestOthersEqual() {
    ChannelEnum tmpDetermineLargestDiff = medianCutColorQuantization.determineLargestDiff(1, 0, 0);
    assertEquals(ChannelEnum.RED, tmpDetermineLargestDiff);
  }

  @Test
  public void testDetermineLargestSecondLargestOthersEqual() {
    ChannelEnum tmpDetermineLargestDiff = medianCutColorQuantization.determineLargestDiff(0, 1, 0);
    assertEquals(ChannelEnum.GREEN, tmpDetermineLargestDiff);
  }

  @Test
  public void testDetermineLargestThirdLargestOthersEqual() {
    ChannelEnum tmpDetermineLargestDiff = medianCutColorQuantization.determineLargestDiff(0, 0, 1);
    assertEquals(ChannelEnum.BLUE, tmpDetermineLargestDiff);
  }

  @Test
  public void testAverageColorList() {
    ArrayList<CustomColor> tmpColors = new ArrayList<>();
    tmpColors.add(new CustomColor(100, 0, 0));
    tmpColors.add(new CustomColor(0, 100, 0));

    CustomColor tmpAverageColor = medianCutColorQuantization.averageColorList(tmpColors);
    assertNotNull(tmpAverageColor);
    assertEquals(new CustomColor(50, 50, 0), tmpAverageColor);
  }

  @Test
  public void testAverageSecondColorList() {
    ArrayList<CustomColor> tmpColors = new ArrayList<>();
    tmpColors.add(new CustomColor(10, 10, 10));
    tmpColors.add(new CustomColor(30, 30, 30));

    CustomColor tmpAverageColor = medianCutColorQuantization.averageColorList(tmpColors);
    assertNotNull(tmpAverageColor);
    assertEquals(new CustomColor(20, 20, 20), tmpAverageColor);
  }

  @Test
  public void testFindDominantColors() throws IOException {
    List<CustomColor> tmpColors = loadImageAndReturnColors(IMAGE_PIXEL_RED);
    List<CustomColor> tmpDominantColors = medianCutColorQuantization.getDominantColors(tmpColors, 1, 10);
    assertEquals(1, tmpDominantColors.size());
  }


  @Test
  public void testFindDominantColorsTwo() throws IOException {
    List<CustomColor> tmpColors = loadImageAndReturnColors(IMAGE_BMP);
    List<CustomColor> tmpDominantColors = medianCutColorQuantization.getDominantColors(tmpColors, 2, 10);
    assertEquals(2, tmpDominantColors.size());
  }

  @Test
  public void testFindDominantColorsFour() throws IOException {
    List<CustomColor> tmpColors = loadImageAndReturnColors(IMAGE_BMP);
    List<CustomColor> tmpDominantColors = medianCutColorQuantization.getDominantColors(tmpColors, 4, 10);
    assertEquals(4, tmpDominantColors.size());
  }

  @Test
  public void testFindDynamicChannelRed() throws IOException {
    String tmpFileName = IMAGE_RANGE_RED;
    List<CustomColor> tmpColors = loadImageAndReturnColors(tmpFileName);

    ChannelEnum tmpBroadestSpectrum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertEquals(ChannelEnum.RED, tmpBroadestSpectrum);
  }

  @Test
  public void testFindDynamicChannelGreen() throws IOException {
    String tmpFileName = IMAGE_RANGE_GREEN;
    List<CustomColor> tmpColors = loadImageAndReturnColors(tmpFileName);

    ChannelEnum tmpBroadestSpectrum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertEquals(ChannelEnum.GREEN, tmpBroadestSpectrum);
  }

  @Test
  public void testFindDynamicChannelBlue() throws IOException {
    String tmpFileName = IMAGE_RANGE_BLUE;
    List<CustomColor> tmpColors = loadImageAndReturnColors(tmpFileName);

    ChannelEnum tmpBroadestSpectrum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertEquals(ChannelEnum.BLUE, tmpBroadestSpectrum);
  }

  @Test
  public void testFindDynamicChannelAll() throws IOException {
    String tmpFileName = "range_all.png";
    List<CustomColor> tmpColors = loadImageAndReturnColors(tmpFileName);

    ChannelEnum tmpBroadestSpectrum = medianCutColorQuantization.findBroadestSpectrum(tmpColors);
    assertEquals(null, tmpBroadestSpectrum);
  }

  private BufferedImage loadImageFromResource(String aFileName) throws IOException {
    InputStream tmpInputStream = getClass().getClassLoader().getResourceAsStream(aFileName);
    BufferedImage tmpImage = ImageIO.read(tmpInputStream);
    return tmpImage;
  }

  private List<CustomColor> loadImageAndReturnColors(String aFileName) throws IOException {
    BufferedImage tmpImage = loadImageFromResource(aFileName);
    ArrayList<CustomColor> tmpColors = new ArrayList<>();

    int tmpWidth = tmpImage.getWidth();
    int tmpHeight = tmpImage.getHeight();

    for (int i = 0; i < tmpWidth; i++) {
      for (int j = 0; j < tmpHeight; j++) {
	int tmpRgb = tmpImage.getRGB(i, j);
	CustomColor tmpColor = new CustomColor(tmpRgb);
	tmpColors.add(tmpColor);
      }
    }
    return tmpColors;
  }

  @Test
  public void testSubList() {
    ArrayList<String> tmpArrayList = new ArrayList<>();
    tmpArrayList.add("a");
    tmpArrayList.add("b");

    List<String> tmpSubList1 = tmpArrayList.subList(0, 1);
    String tmpA = tmpSubList1.get(0);
    assertEquals("a", tmpA);

    List<String> tmpSubList2 = tmpArrayList.subList(1, 2);
    String tmpB = tmpSubList2.get(0);
    assertEquals("b", tmpB);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSplitListEmpty() {
    medianCutColorQuantization.splitListInHalf(new ArrayList<CustomColor>());
  }

  @Test
  public void testSplitListEven() {
    ArrayList<CustomColor> tmpColorList = new ArrayList<CustomColor>();
    tmpColorList.add(new CustomColor(0xFF0000));
    tmpColorList.add(new CustomColor(0x00FF00));
    List<List<CustomColor>> tmpSplitListInHalf = medianCutColorQuantization.splitListInHalf(tmpColorList);
    assertNotNull(tmpSplitListInHalf);
    assertEquals(2, tmpSplitListInHalf.size());
    assertEquals(1, tmpSplitListInHalf.get(0).size());
    assertEquals(1, tmpSplitListInHalf.get(1).size());
  }

  @Test
  public void testSplitListOdd() {
    ArrayList<CustomColor> tmpColorList = new ArrayList<CustomColor>();
    tmpColorList.add(new CustomColor(0xFF0000));
    tmpColorList.add(new CustomColor(0x00FF00));
    tmpColorList.add(new CustomColor(0x0000FF));
    List<List<CustomColor>> tmpSplitListInHalf = medianCutColorQuantization.splitListInHalf(tmpColorList);
    assertNotNull(tmpSplitListInHalf);
    assertEquals(2, tmpSplitListInHalf.size());
    assertEquals(2, tmpSplitListInHalf.get(0).size());
    assertEquals(1, tmpSplitListInHalf.get(1).size());
  }

  @Test
  public void testImage() throws Exception {
    List<CustomColor> tmpColorList = new ArrayList<>();
    tmpColorList.add(new CustomColor(54, 38, 28));
    tmpColorList.add(new CustomColor(213, 193, 135));
    tmpColorList.add(new CustomColor(110, 205, 223));
    tmpColorList.add(new CustomColor(130, 123, 58));
    tmpColorList.add(new CustomColor(42, 125, 148));
    tmpColorList.add(new CustomColor(157, 176, 121));
    tmpColorList.add(new CustomColor(130, 121, 109));
    tmpColorList.add(new CustomColor(167, 198, 218));
    // tmpColorList.add(new CustomColor(213, 76, 6));

    List<CustomColor> tmpColors = loadImageAndReturnColors(IMAGE_JPG);
    List<CustomColor> tmpDominantColors = medianCutColorQuantization.getDominantColors(tmpColors, tmpColorList.size(), 5);
    assertEquals(tmpColorList.size(), tmpDominantColors.size());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetDominantColorBucketsWithNumberOfBucketsZero() throws Exception {
    List<CustomColor> tmpColors = new ArrayList<CustomColor>();
    tmpColors.add(new CustomColor(0xFF0000));
    tmpColors.add(new CustomColor(0x00FF00));
    medianCutColorQuantization.getDominantColorBuckets(tmpColors, 0);
  }
  
  @Test
  public void testGetDominantColorBucketsWithNumberOfBucketsLargerThanColorList() throws Exception {
    List<CustomColor> tmpColors = new ArrayList<CustomColor>();
    tmpColors.add(new CustomColor(0xFF0000));
    tmpColors.add(new CustomColor(0x00FF00));
    List<List<CustomColor>> tmpDominantColorBuckets = medianCutColorQuantization.getDominantColorBuckets(tmpColors, 3);
    int tmpSize = tmpDominantColorBuckets.size();
    assertEquals(2, tmpSize);
  }

}
